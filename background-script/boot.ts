import { ConnectBackgroundListener } from './connect-listener';
import { RuntimeBackgroundListener } from './runtime-listener';

const runtimeBackgroundListener = new RuntimeBackgroundListener();
