
export class RuntimeContentListener {

    constructor() {
        // Comento esto porque no le veo el sentido, al hacer el ping asincrono siempre va a devolver falso...
        // Esto se podría cambiar a una promesa y en la resolción de la misma actuar en consecuencia para darle sentido asincrono
        // Igualmente no le veo el sentido, quizás es por algún error que se generaba... No lo elimino y cuando vuelva Paco que lo mire
        // if (this.secureContentScriptInitialization()) {
            this.initializeMessagesListener();
            this.initializeScrollListener();
            this.initializeOnLoadEmitter();
            this.initializeBeforeUnloadEmitter();
            this.Delegate(document, "click", "#chrome-extension-scroll-up", function (event) {
                window.scrollTo(0,0);
            });
        // }
    }

    initializeMessagesListener() {

        let me = this;

        chrome.runtime.onMessage.addListener((message, sender, sendResponse) => {

            // console.log(message);
            const command = message;
            var response:any = {};
            // console.log('Received runtime command: ' + command.id);

            switch (command.id) {

                case "connectInit":
                    response = { message: 'OK + ' + command.id };
                    sendResponse(response);
                break;

            }

        });

    }

    initializeScrollListener() {

        let me = this;

        document.addEventListener('scroll', function (e:any) {

            var posicionNueva = window.scrollY;

            if (e.target.scrollTop) {
                posicionNueva = e.target.scrollTop
            }

            if (posicionNueva > 0) {
                if (document.body.contains(document.getElementById('chrome-extension-scroll-up'))) {

                } else {
                    me.MostrarUp();
                }
            } else {
                me.OcultarUp();
            }

        }, true);

        window.addEventListener("focus", function (event) {



        }, false);


    }

    initializeOnLoadEmitter() {

        let me = this;

    }

    initializeBeforeUnloadEmitter () {

        let me = this;
        window.onbeforeunload = function (e) {
            chrome.runtime.sendMessage({ type: "beforeUnload", message: 0 });
        };

    }

    MostrarUp() {

        let html_up = '<span id="chrome-extension-scroll-up" style="position: fixed; z-index: 999999999; width: 50px; height: 50px; right: 50px; bottom: 50px;">';
        html_up += '<img src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pg0KPCEtLSBHZW5lcmF0b3I6IEFkb2JlIElsbHVzdHJhdG9yIDE2LjAuMCwgU1ZHIEV4cG9ydCBQbHVnLUluIC4gU1ZHIFZlcnNpb246IDYuMDAgQnVpbGQgMCkgIC0tPg0KPCFET0NUWVBFIHN2ZyBQVUJMSUMgIi0vL1czQy8vRFREIFNWRyAxLjEvL0VOIiAiaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkIj4NCjxzdmcgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCINCgkgd2lkdGg9IjYxMnB4IiBoZWlnaHQ9IjYxMnB4IiB2aWV3Qm94PSIwIDAgNjEyIDYxMiIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgNjEyIDYxMjsiIHhtbDpzcGFjZT0icHJlc2VydmUiPg0KPGc+DQoJPGcgaWQ9Il94MzZfXzJfIj4NCgkJPGc+DQoJCQk8cGF0aCBkPSJNMzA2LDBDMTM3LjAxMiwwLDAsMTM2Ljk5MiwwLDMwNnMxMzcuMDEyLDMwNiwzMDYsMzA2YzE2OS4wMDgsMCwzMDYtMTM3LjAxMiwzMDYtMzA2UzQ3NS4wMDgsMCwzMDYsMHogTTQzMS4wMDEsMzE2LjIzMQ0KCQkJCWMtNy40NzgsNy40NzktMTkuNTg0LDcuNDc5LTI3LjA0MywwbC03OC44MzMtNzguODEzdjIwMi40NTdjMCwxMC41NTctOC41NjgsMTkuMTI1LTE5LjEyNSwxOS4xMjUNCgkJCQljLTEwLjU1NywwLTE5LjEyNS04LjU2OC0xOS4xMjUtMTkuMTI1VjIzNy40MThsLTc4LjgxNCw3OC44MTNjLTcuNDc4LDcuNDc5LTE5LjU4NCw3LjQ3OS0yNy4wNDMsMA0KCQkJCWMtNy40NzgtNy40NzgtNy40NzgtMTkuNTgzLDAtMjcuMDQybDEwOC4xOS0xMDguMTljNC41NzEtNC41NzEsMTAuODYzLTYuMDA1LDE2Ljc5Mi00Ljk1Mw0KCQkJCWM1LjkyOS0xLjA1MiwxMi4yMjEsMC4zODIsMTYuODExLDQuOTUzbDEwOC4xOSwxMDguMTlDNDM4LjQ2LDI5Ni42NjcsNDM4LjQ2LDMwOC43NzMsNDMxLjAwMSwzMTYuMjMxeiIvPg0KCQk8L2c+DQoJPC9nPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPC9zdmc+DQo=" width="50" height="50">';
        html_up += '</span>';

        document.body.innerHTML += html_up;

    };


    OcultarUp() {
        var item = document.getElementById('chrome-extension-scroll-up');
        item.parentNode.removeChild(item);
    };


    Delegate(el, evt, sel, handler) {
        el.addEventListener(evt, function (event) {
            var t = event.target;
            while (t && t !== this) {
                if (t.matches(sel)) {
                    handler.call(t, event);
                }
                t = t.parentNode;
            }
        });
    }


}
