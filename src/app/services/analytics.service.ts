import { Injectable } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
declare let ga: Function;

@Injectable()
export class AnalyticsService {

    constructor(public router: Router) {

        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = (1 * Number(new Date())); a = s.createElement(o),
                m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga'); // Note: https protocol here

        ga('create', 'UA-144501068-1', 'auto');
        ga('set', 'checkProtocolTask', function () { }); // Removes failing protocol check. @see: http://stackoverflow.com/a/22152353/1958200
        ga('require', 'displayfeatures');

    }


    /**
     * Emit google analytics event
     * Fire event example:
     * this.emitEvent("testCategory", "testAction", "testLabel", 10);
     * @param {string} eventCategory
     * @param {string} eventAction
     * @param {string} eventLabel
     * @param {any} eventValue
     */
    public emitEvent(eventCategory: string,
        eventAction: string,
        eventLabel: string = null,
        eventValue: any = null) {
        if (typeof ga === 'function') {
            ga('send', {
                hitType: eventCategory,
                eventCategory: eventLabel,
                eventAction: eventAction,
                eventLabel: eventValue
            });
        }
    }
    /**
     * Emit google analytics view
     * Fire event example:
     * this.emiView("index");
     * @param {string} viewString
     */
    public emitView(viewString: string) {
        ga('send', 'pageview', viewString + '.html');
    }


}

