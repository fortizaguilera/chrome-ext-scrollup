import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ChromeService {

    constructor() { }

    getChromeStorageObj(key:string) {
        return (chrome.storage.sync.get(key, function (result) {
            return result;
        }));
    }

    setChromeStorageObj(key: string, obj:any) {
        chrome.storage.sync.set({ key : obj }, function () {});
    }

    sendMessage(type: string, message:string) {
        chrome.runtime.sendMessage({ type: type, message: message });
    }

}
