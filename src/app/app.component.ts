import { Component, OnInit, ViewEncapsulation  } from '@angular/core';
import { AnalyticsService } from './services/analytics.service';

declare var $: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None,
    providers: [
        AnalyticsService
    ]
})


export class AppComponent implements OnInit  {

    title = 'zespri-ng';

    constructor(public analyticsService: AnalyticsService) { }

    ngOnInit() {
        chrome.browserAction.setBadgeText({
            'text': '' //an empty string displays nothing!
        });
        chrome.runtime.sendMessage(
            { type: "checkFirstOpen"}, 
            function (response) { 
                $('.empty-loader').remove();
                let time = 1500;
                if(response.firstOpen === true){
                    setTimeout(function () {
                        $('.loader').fadeOut(500, function () { $(this).remove(); });
                    }, time);
                }
                else{
                    $('.loader').remove();
                }
                
            }
        );
        
        this.analyticsListener();
    }
    

    analyticsListener() {

        let me = this;

        document.addEventListener('DOMContentLoaded', function () {
            var a_element = document.querySelectorAll('a');
            for (var i = 0; i < a_element.length; i++) {
                a_element[i].addEventListener('click', function (e: any) {
                    var target = e.target.id;
                    me.analyticsService.emitView(target + '.html');
                    me.analyticsService.emitEvent('event', 'Menu', 'Click', target);
                });
            }
        });
    }

}
