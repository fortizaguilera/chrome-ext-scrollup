// app-routing.component.ts

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PopupComponent } from './components/popup/popup.component';

const routes: Routes = [
    { path: '**', pathMatch: 'full', redirectTo: 'popup' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
