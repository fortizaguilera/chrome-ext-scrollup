import { Component, OnInit } from '@angular/core';


declare var $: any;

@Component({
    selector: 'app-popup',
    templateUrl: './popup.component.html',
    styleUrls: ['./popup.component.scss']
})
export class PopupComponent implements OnInit {




    constructor( ) {
        //this.chromeScrollListener();
    }

    ngOnInit() {

        this.connectBackgroundMessage();
        this.popupEvents();
        chrome.runtime.connect();;
    }

    connectWithContentScript = () => {
        const tabQueryData = { active: true, currentWindow: true };
        chrome.tabs.query(tabQueryData, (tabs) => {
            const port = chrome.tabs.connect(tabs[0].id);
            port.postMessage({id:'Hi'});
            port.onMessage.addListener((response) => {
                // console.log('Content script responded: ' + response);
            });
        });
    }

    connectBackgroundMessage() {
        chrome.runtime.sendMessage({ type: "connectBackgroundInit", message: { id: 'connectBackgroundInit' } }, function (response) {
            // console.log(response);
        });
    }



    popupEvents() {
        $('.nav-link').removeClass('active');
        $('#footer-menu-popup').addClass('active');
    }




}
