"use strict"

let gulp = require('gulp');
let ts = require('gulp-typescript');
let exec = require('child_process').exec;
let browserify = require("browserify");
let source = require('vinyl-source-stream');
let buffer = require('vinyl-buffer');
let uglify = require('gulp-uglify');
let sourcemaps = require('gulp-sourcemaps');
let tsify = require("tsify");

gulp.task('ng-build', (cb) => {
    console.log('running ng build...');
    exec('ng build', function (err, stdout, stderr) {
        console.log(stdout);
        console.log(stderr);
        cb(err);
        return true;
    });
});

gulp.task('content-script', () =>  {
    return browserify({
        basedir: '.',
        debug: true,
        entries: ['content-script/boot.ts'],
        cache: {},
        packageCache: {}
    })
        .plugin(tsify)
        .transform('babelify', {
            presets: ['es2015'],
            extensions: ['.ts']
        })
        .bundle()
        .pipe(source('content-script.js'))
        .pipe(buffer())
        .pipe(sourcemaps.init({ loadMaps: true }))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('./dist/scroll-up'));
});

gulp.task('background-script', () =>  {
    return browserify({
        basedir: '.',
        debug: true,
        entries: ['background-script/boot.ts'],
        cache: {},
        packageCache: {}
    })
        .plugin(tsify)
        .transform('babelify', {
            presets: ['es2015'],
            extensions: ['.ts']
        })
        .bundle()
        .pipe(source('background-script.js'))
        .pipe(buffer())
        .pipe(sourcemaps.init({ loadMaps: true }))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('./dist/scroll-up'));
});


gulp.task('ng-build-prod', (cb) => {
    console.log('running ng build...');
    exec('ng build --prod', function (err, stdout, stderr) {
        console.log(stdout);
        console.log(stderr);
        cb(err);
        return true;
    });
});

gulp.task('content-script-prod', () => {
    return browserify({
        basedir: '.',
        debug: true,
        entries: ['content-script/boot.ts'],
        cache: {},
        packageCache: {}
    })
        .plugin(tsify)
        .transform('babelify', {
            presets: ['es2015'],
            extensions: ['.ts']
        })
        .bundle()
        .pipe(source('content-script.js'))
        .pipe(buffer())
        .pipe(uglify())
        .pipe(gulp.dest('./dist/scroll-up'));
});

gulp.task('background-script-prod', () => {
    return browserify({
        basedir: '.',
        debug: true,
        entries: ['background-script/boot.ts'],
        cache: {},
        packageCache: {}
    })
        .plugin(tsify)
        .transform('babelify', {
            presets: ['es2015'],
            extensions: ['.ts']
        })
        .bundle()
        .pipe(source('background-script.js'))
        .pipe(buffer())
        .pipe(uglify())
        .pipe(gulp.dest('./dist/scroll-up'));
});


gulp.task('default', gulp.parallel('ng-build', 'content-script', 'background-script'));
gulp.task('pro', gulp.parallel('ng-build-prod', 'content-script-prod', 'background-script-prod'));
